* 2020-2021: NBA_SEARCH
    - [Grammatical errors in Stat Node response](https://github.com/skekre98/NBA-Search/issues/123)
    - [Improve documentation in Stat Node class](https://github.com/skekre98/NBA-Search/issues/115)
    - [Improve documentation in Inference Network class](https://github.com/skekre98/NBA-Search/issues/113)
    - [Adding REST APIs](https://github.com/skekre98/NBA-Search/issues/153)